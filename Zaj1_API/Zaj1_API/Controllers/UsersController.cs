﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zaj1_API.Model;
using Zaj1_API.Service;

namespace Zaj1_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult<User> Authenticate(User user)
        {
            var u = _userService.Authenticate(user.UserName, user.Password);
            if (u == null)
            {
                return Unauthorized();
            }

            return u;
        }

        public ActionResult<IEnumerable<User>> GetAll()
        {
            return Ok(_userService.GetAll());
        }

        [HttpPut]
        public ActionResult<User> AddUser(User u)
        {
            var user = _userService.AddUser(u);
            if (user != null)
            {
                return Ok(user);
            }

            return BadRequest();
        }
    }
}