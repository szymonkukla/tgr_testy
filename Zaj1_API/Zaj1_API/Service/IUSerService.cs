﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zaj1_API.Model;

namespace Zaj1_API.Service
{
    public interface IUserService
    {
        User Authenticate(string userName, string password);
        IEnumerable<User> GetAll();
        User AddUser(User u);
    }
}
