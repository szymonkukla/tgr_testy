﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Zaj1_API.Model;

namespace Zaj1_API.Service
{
    public class UserService : IUserService
    {

        private List<User> _users = new List<User>
        {
            new User
            {
                Id = 0, FirstName = "Test", LastName = "User", UserName = "user", Password = "Test"
            }
        };
        private AppSettings _appSettings;
        private AppDataContext _appDataContext;

        public UserService(IOptions<AppSettings> appSettings, AppDataContext appDataContext)
        {
            _appSettings = appSettings.Value;
            _appDataContext = appDataContext;
        }

        public User AddUser(User u)
        {
            _appDataContext.Users.Add(u);
            _appDataContext.SaveChanges();
            return u;
        }

        public User Authenticate(string userName, string password)
        {
            var user = _users.SingleOrDefault(x => x.UserName == userName && x.Password == password);

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim("LastName", user.FirstName),
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            user.Password = null;

            return user;
        }

        public IEnumerable<User> GetAll()
        {
            return _users.Select(u =>
            {
                u.Password = string.Empty;
                return u;
            });
        }
    }
}
