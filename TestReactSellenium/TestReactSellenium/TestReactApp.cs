﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace TestReactSellenium
{
    public class TestReactApp
    {
        private ChromeDriver _driver;
        [SetUp]
        public void Init()
        {
            _driver = new ChromeDriver("D:\\Code\\TGR_Testy\\TestReactSellenium\\TestReactSellenium\\bin\\Debug\\netcoreapp2.2");
        }

        [TearDown]
        public void DeInit()
        {
            //_driver.Close();
        }

        [Test]
        public void TestLoadingVideos()
        {
            _driver.Url = "http://localhost:3000/";
            var searchBox = _driver.FindElement(By.XPath("//input[@type=\"text\"]"));
            searchBox.SendKeys("trial bike");
            var button = _driver.FindElement(By.XPath("//button"));
            button.Click();
        }
    }
}
