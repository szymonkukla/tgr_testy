﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TestReactSellenium
{
    class ParallelTest
    {
        private List<ChromeDriver> _drivers;
        [SetUp]
        public void Init()
        {
            _drivers = new List<ChromeDriver>();
            for (int i = 0; i < 5; i++)
            {
                _drivers.Add(new ChromeDriver("D:\\Code\\TGR_Testy\\TestReactSellenium\\TestReactSellenium\\bin\\Debug\\netcoreapp2.2"));
            }
            // ChromeDriver("D:\\Code\\TGR_Testy\\TestReactSellenium\\TestReactSellenium\\bin\\Debug\\netcoreapp2.2");
        }

        [TearDown]
        public void DeInit()
        {
            //_driver.Close();
        }

        [Test]
        public void TestLoadingVideos()
        {
            List<Task> TaskList = new List<Task>();
            for (int i = 0; i < 5; i++)
            {
                TaskList.Add(() => TestAct);
            }
        }

        private TestAct()
        {
            var driver = new ChromeDriver("D:\\Code\\TGR_Testy\\TestReactSellenium\\TestReactSellenium\\bin\\Debug\\netcoreapp2.2");
            driver.Url = "http://localhost:3000/";
            var searchBox = driver.FindElement(By.XPath("//input[@type=\"text\"]"));
            searchBox.SendKeys("trial bike");
            var button = driver.FindElement(By.XPath("//button"));
            button.Click();
        }
    }
}
