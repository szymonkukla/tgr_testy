Feature: App should say "Is weekend!" or "Is not weekend :("

Scenario Outline: Check is weekend
    Given Is weekend on <day>?
    Then App should say <message>

Examples:
    |   day     |   message       |
    |   1      |   'Is not weekend :(' |
    |   2      |   'Is not weekend :(' |
    |   3      |   'Is not weekend :(' |
    |   4      |   'Is not weekend :(' |
    |   5      |   'Is not weekend :(' |
    |   6       |   'Is weekend!' |
    |   7       |   'Is weekend!' |