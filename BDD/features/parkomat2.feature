Feature: Parkomat should give correct rest and say 'Not enought money' or 'Thank You'

Scenario Outline: General Parkomat
    Given I have <money> PLN
    Given the cost per hour is <costPerHour> PLN
    When I want to stay <noHours> hours
    Then Parkomat should return <rest> PLN
    Then Parkomat should say <message>

Examples:
    |   money   |   costPerHour |   noHours    |    rest    |   message     |
    |   50      |   3           |   10         |    20      |   "Thank You" |
    |   10      |   3           |   2          |    4       |   "Thank You" |
    |   20      |   3           |   10         |    -10     |   "Not enought money" |