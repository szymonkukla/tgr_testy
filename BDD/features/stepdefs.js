const assert = require('assert');
const { Given, When, Then } = require('cucumber');

const calculateRest = (money, costPerHour, noHours) => {
  const rest = money - costPerHour * noHours;

  return {
    rest: rest,
    message: rest < 0 ? 'Not enought money' : 'Thank You'
  };
}

Given('I have {int} PLN', function (money) {
  this.money = money;
});

Given('the cost per hour is {int} PLN', function (costPerHour) {
  this.costPerHour = costPerHour;
});

When('I want to stay {int} hours', function (noHours) {
  this.rest = calculateRest(this.money, this.costPerHour, noHours);
});

Then('Parkomat should return {int} PLN', function (rest) {
  assert.equal(this.rest.rest, rest);
});

Then('Parkomat should say {string}', function (str) {
  assert.equal(this.rest.message, str);
});

const checkWeekend = (day) => {
  if (day === 6 || day === 7) {
    return 'Is weekend!';
  }

  return 'Is not weekend :(';
}
Given('Is weekend on {int}?', function (day) {
  this.day = day;
  });

Then('App should say {string}', function (str) {
  assert.equal(checkWeekend(this.day), str);
});