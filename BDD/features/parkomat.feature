Feature: Parkomat should return a correct rest

Scenario: Parkomat should return 16 PLN
    Given I have 20 PLN
    Given the cost per hour is 2 PLN
    When I want to stay 2 hours
    Then Parkomat should return 16 PLN

Scenario: Parkomat should return 0 PLN
    Given I have 20 PLN
    Given the cost per hour is 2 PLN
    When I want to stay 10 hours
    Then Parkomat should return 0 PLN

Scenario: Parkomat should return negative and say "Not enought money"
    Given I have 10 PLN
    Given the cost per hour is 2 PLN
    When I want to stay 10 hours
    Then Parkomat should return -10 PLN
    Then Parkomat should say "Not enought money"