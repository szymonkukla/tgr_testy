import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  constructor(private _router: Router) { }

  public adduser() {
    this._router.navigate(['adduser']);
  }
  ngOnInit() {
  }

}
