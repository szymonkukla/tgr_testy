import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.less']
})
export class AdduserComponent implements OnInit {
  public newUser = {
      firstName: '',
      lastName: '',
      userName: '',
      password: '',
  }
  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  public register() {
    console.log(this.newUser);
    this.http.put('http://localhost:5000/api/users', this.newUser).subscribe(res => {
      console.log(res);
    })
  }

}
