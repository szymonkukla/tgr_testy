/// <reference types="cypress"/>

context("Test google page", () => {
    it("search google for trails bike", () => {
        cy.visit('https://www.google.pl')
        .get('input[type="text"]')
        .type('trail bike')
        .get("form")
        .submit()
        .url().should('include', 'q=');
    })
})